#include <RCSwitch.h>
#define TRANSMITTER_PIN 3
RCSwitch mySwitch = RCSwitch();


#include <SPI.h>
#include <Ethernet.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress serverIP(192, 168, 1, 1);  // dd: numeric IP of server (no DNS)
IPAddress dnsAddress(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress ip(192, 168, 1, 10);

EthernetServer server(80);
String readString;

EthernetClient client;

unsigned long lastConnectionTime = 0;          // last time of connection to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 10*1000;  // delay between updates, in milliseconds

void setup(){
  Serial.begin(9600); 
  Serial.println("Program START"); 

  mySwitch.enableTransmit(TRANSMITTER_PIN);
  mySwitch.setPulseLength(199);
  mySwitch.setRepeatTransmit(30);

  Ethernet.begin(mac, ip, dnsAddress, gateway, subnet);
  // give the ethernet module time to boot up:
  delay(1000);
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
  server.begin();
}

void loop(){
  // if an incoming client connects, there will be bytes available to read:
  EthernetClient client2 = server.available();
  if (client2) {
    while (client2.connected()) {
      if (client2.available()) {
        char c = client2.read();
        if (readString.length() < 100) {
          //store characters to string 
          readString += c;
        }

        //output chars to serial port
        Serial.print(c);
  
        //if HTTP request has ended
        if (c == '\n') {
          readString.toLowerCase();
		  
          //dirty skip of "GET /favicon.ico HTTP/1.1"
          if (readString.indexOf("?") < 0) {
            //skip everything
          }
          else
			  if(readString.indexOf("s") > 0){
				 
				 String signalString = String(100);
				 int ind1 = 0;
				 int ind2 = 0;
				 signalString = "";
				 ind1 = readString.indexOf('=');
				 ind2 = readString.indexOf('http');
				 //Serial.println(ind1);
				 //Serial.println(ind2);
				 char signalChar[100];
				 signalString = readString.substring(ind1+1, ind2-4);
				 signalString.toCharArray(signalChar,100);
				 //Serial.println(signalString);
				 mySwitch.send(signalChar);
			  }

            //now output HTML data header
          if(readString.indexOf('?') >=0) {
            client2.println(F("HTTP/1.1 204 Controller"));
            client2.println();
            client2.println();  
          }

          delay(1);
          client2.stop();
          readString = "";
        }          
      }
    }
  }
}
