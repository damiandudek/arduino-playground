/*
 * dht11_module.ino
 *
*/

//====================================================
// CONFIGRUATION
//----------------------------

#define HTTP_REQUEST_FILE "req.php"

//----------------------------
// DHT11 Humidity & Temperature Sensor
//----------------------------

#include <dht11.h>

#define DHT_PIN 7
#define DHT11_TIME_BETWEEN_READINGS 30000  

DHT11 dht11(DHT_PIN);  
unsigned long dhtTimer; 

//----------------------------
// LCD i2c converter
//----------------------------

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define BACKLIGHT_PIN 3
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);

//----------------------------
// ETHERNET
//----------------------------

#include <SPI.h>
#include <Ethernet.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress serverIP(192, 168, 1, 1);  // IP addres of server (no DNS)
IPAddress dnsAddress(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress ip(192, 168, 1, 10);

EthernetClient client;
//----------------------------


void setup(){
  Serial.begin(9600);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Arduino Leonardo only
  }
  
  Serial.println("Initalization..."); 

  // LCD i2C
  lcd.begin(16, 2);  // 
  lcd.setBacklightPin(BACKLIGHT_PIN, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.clear();
  lcd.home();
  lcd.print("Initalization...");  

  dhtTimer = -5000;
  
  Ethernet.begin(mac, ip, dnsAddress, gateway, subnet);
  // give the ethernet module time to boot up:
  delay(1000);
  // print the Ethernet board/shield's IP address:
  Serial.print("Arduino IP address: ");
  Serial.println(Ethernet.localIP());
}


// this method makes a HTTP connection to the server:
void httpRequest(String url, String file, String params) {
  if (client.connect(serverIP, 80)) {
    
    Serial.println("Connected");
    
    // send the HTTP GET request:
    client.print("GET ");
    client.print(url);
    client.print(file);
    client.print(params);
    client.println(" HTTP/1.1");
    client.println("Host: www.arduino.cc");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
  } 
  else {
    Serial.println("Connection failed");
    Serial.println("Disconnecting");
    client.stop();
  }
}

char buffer[60];  // buffer for the request data
int8_t errorCode;

int8_t dht11read(){  
  if (millis() > dhtTimer + DHT11_TIME_BETWEEN_READINGS){
    dhtTimer = millis();
    
    if((errorCode = dht11.measure()) == 0){
      // prepare data to send
      sprintf(buffer, "?humidity=%d&temperature=%d", (int)dht11.humidity, (int)dht11.temperature);

      httpRequest("/", HTTP_REQUEST_FILE, buffer);
      Serial.println("Readings sent to server");
      Serial.println("Disconnecting");
      client.stop();
      
      Serial.print("temperature:");
      Serial.print(dht11.temperature);
      Serial.print("C");        
      Serial.print(" humidity:");
      Serial.print(dht11.humidity);
      Serial.println("%");  
      return true;  
    }
    else{
      Serial.println();
      Serial.print("Error Code:");
      Serial.print(errorCode);
      Serial.println();    
    }
  }
  return false;
}

void sendToLCD(){
    // display humidity and temperature on the LCD
    lcd.clear();
    lcd.print("Temp: ");  
    lcd.print(dht11.temperature);
    lcd.print((char)223);
    lcd.print("C");
    lcd.setCursor(0, 1);
    lcd.print ("Humi: ");
    lcd.print (dht11.humidity);
    lcd.print("%");
}

void loop(){
  if(dht11read())
    sendToLCD();
}
